﻿using System;
using cfpt_courseLapinTortue;

namespace cfpt_courseLapinTortue_Console
{
    class Program
    {

        static void Main(string[] args)
        {
            new Program();
        }

        private CourseLapinTortueModel _model;
        
        public Program()
        {
            _model = new CourseLapinTortueModel();

            _model.AjouterJoueur(new Lapin("Lapin  1", 0));
            _model.AjouterJoueur(new Tortue("Tortue 1", 0));
            
            Console.WriteLine("=============================================");
            Console.WriteLine("| Jeu de la course du lapin et de la tortue |");
            Console.WriteLine("=============================================");
            
            Console.WriteLine("A chaque tirage :");
            Console.WriteLine("    La tortue avance de 1, sauf si c'est un 6.");
            Console.WriteLine("    Le lapin avance de 6, seulement si c'est un 6.");
            
            while (!_model.LaPartieEstTerminee())
            {
                _model.Tirage();
                
                Console.WriteLine($"Tirage No : {_model.NumeroTirage}");
                foreach (Joueur joueur in _model.LstJoueurs)
                {
                    Console.WriteLine($"    {joueur}");
                }
            }
            
            Console.WriteLine("Classement");
            Console.WriteLine("==========");
            
            Console.WriteLine($"Course en {_model.NombreCase} cases");
            Console.WriteLine($"Nombre de tirage : {_model.NumeroTirage}");
            
            Console.WriteLine(_model.AfficheClassement());

            Console.ReadKey();
        }
    }
}