﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace cfpt_courseLapinTortue
{
    public class CourseLapinTortueModel
    {
        private List<Joueur> _lstJoueurs;
        private int _nombreCase;
        private int _nombreLapin;
        private int _nombreTortue;
        private int _numeroTirage;

        public List<Joueur> LstJoueurs => _lstJoueurs;

        public int NombreCase => _nombreCase;

        public int NombreLapin => _nombreLapin;

        public int NombreTortue => _nombreTortue;

        public int NumeroTirage => _numeroTirage;

        public CourseLapinTortueModel() : this( 20)
        {
        }

        public CourseLapinTortueModel(int nombreCase)
        {
            _numeroTirage = 0;
            _lstJoueurs = new List<Joueur>();
            _nombreCase = nombreCase;
            _nombreLapin = 0;
            _nombreTortue = 0;
        }

        public void AjouterJoueur(Joueur joueur)
        {
            _lstJoueurs.Add(joueur);
            
            if (joueur.GetType() == typeof(Lapin)) _nombreLapin += 1;
            else if (joueur.GetType() == typeof(Tortue)) _nombreTortue += 1;
        }

        public void SupprimerJoueur(Joueur joueur)
        {
            if (_lstJoueurs.Contains(joueur))
            {
                _lstJoueurs.Remove(joueur);

                if (joueur.GetType() == typeof(Lapin)) _nombreLapin -= 1;
                else if (joueur.GetType() == typeof(Tortue)) _nombreTortue -= 1;
            }
        }

        public string AfficheClassement()
        {
            _lstJoueurs.Sort((a, b) => b.Position - a.Position);
            int i = 1;
            return String.Join("\n", _lstJoueurs.Select(joueur =>
            {
                string ret = $"Rang No {i} :\n{joueur.Nom} ({joueur.Position})";
                i++;
                return ret;
            }));
        }

        public bool LaPartieEstTerminee()
        {
            return _lstJoueurs.Find(joueur => joueur.Position >= NombreCase) != null;
        }

        public void Tirage()
        {
            _numeroTirage += 1;
            _lstJoueurs.ForEach(joueur => joueur.Joue());
        }

        public override string ToString()
        {
            return $"";
        }
    }
}