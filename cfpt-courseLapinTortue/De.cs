﻿using System;

namespace cfpt_courseLapinTortue
{
    public class De
    {
        public const int VAL_MAX = 6;
        public const int VAL_MIN = 1;
        
        private int _valeur;
        private Random _random;

        public int Valeur => _valeur;

        public De()
        {
            _random = new Random();
        }

        public De(int valeur) : this()
        {
            _valeur = valeur;
        }

        public void Roule()
        {
            _valeur = _random.Next(VAL_MIN, VAL_MAX+1);
        }

        public override string ToString()
        {
            return $"Valeur actuelle du dé : {_valeur}";
        }
    }
}