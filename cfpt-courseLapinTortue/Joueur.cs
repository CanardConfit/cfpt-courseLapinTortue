﻿using System.Collections.Generic;

namespace cfpt_courseLapinTortue
{
    public delegate void JoueurBougeEventHandler(Joueur sender, bool positionChanged);
    public abstract class Joueur
    {
        protected List<int> ListAutorise;
        protected int EtapeBouge;
        
        protected De _deJoueur;
        protected string _nom;
        protected int _position;

        public JoueurBougeEventHandler JoueurBouge;

        public De DeJoueur => _deJoueur;

        public string Nom => _nom;

        public int Position => _position;

        public void Joue()
        {
            _deJoueur.Roule();

            if (ListAutorise.Contains(_deJoueur.Valeur))
            {
                _position += EtapeBouge;
                JoueurBouge?.Invoke(this, true);
            }
            JoueurBouge?.Invoke(this, false);
        }
        
        protected Joueur(string nom, int position)
        {
            ListAutorise = new List<int>();
            _deJoueur = new De();
            _nom = nom;
            _position = position;
        }

        public override string ToString()
        {
            return $"{_nom} est à la position : {_position}   Valeur de son tirage : {_deJoueur.Valeur}";
        }
    }
}