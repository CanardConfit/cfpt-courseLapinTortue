﻿namespace cfpt_courseLapinTortue
{
    public class Lapin : Joueur
    {
        public Lapin() : this("Joueur", 0)
        {
        }

        public Lapin(string nom, int position) : base(nom, position)
        {
            EtapeBouge = 6;
            ListAutorise.AddRange(new[] { 6 });
        }
    }
}