﻿namespace cfpt_courseLapinTortue
{
    public class Tortue : Joueur
    {
        public Tortue() : this("Joueur", 0)
        {
        }

        public Tortue(string nom, int position) : base(nom, position)
        {
            EtapeBouge = 1;
            ListAutorise.AddRange(new[] { 1,2,3,4,5 });
        }
    }
}